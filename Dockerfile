# syntax = docker/dockerfile:1.0-experimental

# Based on the Dockerfile from the meuse repository
# https://github.com/mcorbin/meuse/blob/master/Dockerfile

# BUILDING STAGE
FROM clojure:openjdk-11-lein as build-env

RUN git clone https://github.com/mcorbin/meuse.git /app
WORKDIR /app

RUN lein uberjar

# APPLICATION STAGE
FROM openjdk:16-buster

# This will be the name of the user committing to the registry repo
ARG USER
ARG GIT_USER_NAME
ARG GIT_USER_MAIL
ARG GIT_HOST
ARG GIT_REGISTRY_URL

RUN apt-get update && apt-get -y upgrade

# Add system user meuse and prepare for the build artifact
RUN groupadd -r meuse && useradd -r -s /bin/false -g meuse meuse
RUN mkdir -p /app
RUN chown -R meuse:meuse /app

# Create regular user
RUN useradd -mG meuse $USER
RUN mkdir -p /repos /registry
RUN chown -R $USER:$USER /repos /registry
USER $USER
RUN mkdir -p ~/.ssh

# Configure git
RUN git config --global user.name $GIT_USER_NAME
RUN git config --global user.email $GIT_USER_MAIL

# We need to trust our git repository host
RUN ssh-keyscan -t rsa $GIT_HOST >> ~/.ssh/known_hosts

# When using the docker image (runtime), private_key must be set
# accordingly
RUN ln -s /run/secrets/private_key ~/.ssh/id_rsa

# The private key is passed in from the command line, and is only
# visible during this specific RUN command
# https://docs.docker.com/develop/develop-images/build_enhancements/#new-docker-build-secret-information
RUN --mount=type=secret,id=private_key,dst=/run/secrets/private_key,uid=1000,gid=1000\
        git clone $GIT_REGISTRY_URL /registry

# Fetch the build artifact (when done at the end of the dockerfile,
# commands run before may proceed while waiting for the build stage to
# finish, resulting in faster build time for the entire docker image).
USER meuse
COPY --from=build-env \
        /app/target/uberjar/meuse-*-standalone.jar \
        /app/meuse.jar

USER $USER
ENTRYPOINT ["java"]
CMD ["-jar", "/app/meuse.jar"]
