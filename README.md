# Private hosting of Rust crates

This repo contains configuration files for setting up at private Rust crates
registry. Further information, and instructions of usage, is documented in
[our blog](https://www.satcube.com/private-hosting-of-rust-crates/).
